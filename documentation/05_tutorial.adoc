// [[section-tutorial]]
== Tutorial
=== Parts
[cols="1e,2e",options="header"]
|===
|Part
|Note / we use

|3D printed housing
|.stl files are in the hardware repository

|circuit board
|see <<Circuit Board>> and <<Embedded Software>> Chapter

|battery connectors
|1x Keystone 291 https://www.mouser.de/ProductDetail/534-291[@mouser] (24.01.2022) +
1x Keyston 209 https://www.mouser.de/ProductDetail/534-209[@mouser] (24.01.2022) +
1x Keystone 228 https://www.mouser.de/ProductDetail/534-228[@mouser] (24.01.2022)

|PTFE membran with an diameter of 9.1mm
|PXTPX4 from https://www.pieper-filter.de[Pieper Filter GmbH] +
0.45 µm PTFE with 54 g/m2 polypropylene support body

|3x M2.5 x 12mm screws
|Countersunk Head

|2x Energizer® Ulimate Lithium™ AA
|the device needs > 3V power supply, so Alkaline AA Batteries doesn't work

|sealing ring (OR 74x1.5)
|O-Ring, 74x1.5 mm FKM or NBR

|Nb-IoT nano sim
|look in <<section-appendix>> for the correct config

|===
=== Embedded Software

Here are some examples how to flash the software on the device.

==== Example: Ubuntu 21.04 (min. 15 GB hard drive, 8 GB ram)
This example can be run in a terminal on a fresh Ubuntu 21.04 (Hirsute Hippo) installation. The comments indicate additional steps to take.
```
sudo apt install curl git openocd gcc libusb-1.0-0-dev libftdi-dev
# Rustup installation as recommended on https://www.rust-lang.org/tools/install, please be aware of the security implications
# Please follow the installation instructions for the Rustup setup
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# Re-open the terminal to update the path variable
rustup target add thumbv7em-none-eabihf
cargo install probe-run
# -> Set up SSH key authentication with Gitlab (https://docs.gitlab.com/ee/ssh/)
git clone --recursive git@git.openlogisticsfoundation.org:silicon-economy/devices/sensing-puck/firmware.git
cd sensing-puck-firmware
cargo build
# -> Set up udev rules to allow access to your flasher, i.e. create an appropriate udev rules file in /etc/udev/rules.d or copy a matching one from udev-rules, do not forget to logout and login for the changes to take effect
# -> Connect your flasher, power up your device
cargo run
# The parts below are only required to debug the application
sudo apt install gdb-multiarch
# -> Download the .deb vscode installer from https://code.visualstudio.com/download
sudo dpkg -i code_<version>.deb
# -> In VSCode, install the following plugins: cortex-debug, rust-analyzer. For rust-analyzer, install the "rust-analyzer server"
# -> Run the "Build + Debug (OpenOCD + STLink)" launch configuration

# TODO arm-none-eabi-gdb?!!
```

==== Example: Archlinux (min. 15 GB hard drive, 8 GB ram)
These commands have been tested on a fresh installation of ArchLinux which has been "installed" with Vagrant. It is based on the "archlinux/archlinux" box version "20210415.20050" and was tested on 2021-04-26. As for the Ubuntu example, the comments indicate the additional steps that must be taken.
```
sudo pacman -Syu
sudo pacman -S git gcc pkgconf libusb
# Rustup installation as recommended on https://www.rust-lang.org/tools/install, please be aware of the security implications
# Please follow the installation instructions for the Rustup setup
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# -> Re-open the terminal to update the path variable
rustup target add thumbv7em-none-eabihf
cargo install probe-run
# -> Set up SSH key authentication with Gitlab (https://docs.gitlab.com/ee/ssh/)
git clone --recursive git@git.openlogisticsfoundation.org:silicon-economy/devices/sensing-puck/firmware.git
cd sensing-puck-firmware
cargo build
# -> Set up udev rules to allow access to your flasher, i.e. create an appropriate udev rules file in /etc/udev/rules.d or copy a matching one from udev-rules, do not forget to logout and login for the changes to take effect
# -> Connect your flasher, power up your device
cargo run
# The parts below are only required to debug the application
sudo pacman -S xorg-server xorg-xinit xterm && startx # Installs and starts a graphical user interface, if you are already using ArchLinux, you have probably already done that
sudo pacman -S openocd arm-none-eabi-gdb code
code . # starts VSCode in sensing-puck-firmware directory
# -> In VSCode, install the following plugins: cortex-debug, rust-analyzer. For rust-analyzer, install the "rust-analyzer server"
# -> Run the "Build + Debug (OpenOCD + STLink)" launch configuration
```

=== Assembly
The first step after printing is to clean the housing and remove the support pressures.

Then the battery connector can assemblied in the intended places on the lower part of the housing (see picture).

Next step is to solder the cables to the battery contacts and the circuit board as shown in the picture. Also put the nano sim in the sim card slot on the circuit board.

image::images/05_assembly01.jpg[Assembly Image 1, 50%]

In the next part of the assembly, the PTFE membrane is placed in the upper part of the housing (over the small struts) and the sealing ring is placed in the recess provided in the lower part of the housing.

image::images/05_assembly02.jpg[Assembly Image 2, 45%]
image::images/05_assembly03.jpg[Assembly Image 3, 45%]

Finally, the upper part is placed on the lower part of the housing and fixed with the screws. Do not forget to put the batteries inside before closing the housing.

image::images/05_assembly04.jpg[Assembly Image 4, 50%]

The Sensing Puck is ready to go.
