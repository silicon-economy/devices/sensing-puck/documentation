# Silicon Economy Sensing Puck

In the documentation folder you can find the documentation for the Sensing Puck. It is a device that was developed in the project Modular IoT Devices in the research project Silicon Economy.

### Documentation Overview
  * Use Case & Solution
  * Embedded Hard- & Software
  * Application

### License

Licensed under the Open Logistics Foundation License 1.3.

### Contact information

* Sebastian Wibbeling - sebastian.wibbeling@iml.fraunhofer.de
* Sönke Kauffmann - soenke.kauffmann@iml.fraunhofer.de
